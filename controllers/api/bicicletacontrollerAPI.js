let Bicicleta = require('../../models/bicicleta')


exports.BicicletaList = (req, res) => {
  res.status(200).json({ bicicletas: Bicicleta.allBics })
}

exports.BicicletaCreate = (req, res) => {
  let bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo)
  bici.ubicacion = [req.body.lat, req.body.lng]
  Bicicleta.add(bici)
  res.status(200).json({ bicicleta: bici })
}

exports.BicicletaDelete = (req, res) => {
  Bicicleta.removeById(req.body.id)
  res.status(204).send()
}