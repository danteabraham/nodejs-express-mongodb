let usuario = require('../../models/usuario')


exports.usuariosList = function(req, res) {
  usuario.find({}, function(err, usuarios) {
    res.status(200).json({ usuarios: usuarios })
  })
}

exports.usuariosCreate = function(req, res) {
  let usuario = new Usuario({ nombre: req.body.nombre })
  usuario.save(function() {
    res.status(200).json(usuario)
  })
}

exports.usuariosReservar = function(req, res) {
  Usuario.findById(req.body.id, function(err, usuario) {
    console.log(usuario)
    usuario.reservar(req.body.bici_id, req.body.desde, req.body.hasta, function(err) {
      console.log('reserva')
      res.status(200).send()
    })
  })
}