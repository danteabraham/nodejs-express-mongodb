let mongoose = require('mongoose')
let Schema = mongoose.Schema


var bicicletaSchema = new Schema({
  code: Number,
  color: String,
  modelo: String,
  ubicacion: {
    type: [Number],
    index: { type: '2dsphere', sparse: true }
  }
})

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion) {
  return new this({ code: code, color: color, modelo: modelo, ubicacion: ubicacion })
}

bicicletaSchema.methods.toString = function() {
  return `code: ${this.code} | color: ${this.color}`
}

bicicletaSchema.statics.allBicis = function(cb) {
  return this.find({}, cb)
}

bicicletaSchema.statics.add = function(aBici, cb) {
  this.create(aBici, cb)
}

bicicletaSchema.statics.findByCode = function(aCode, cb) {
  return this.findOne({ code: aCode }, cb)
}

bicicletaSchema.statics.removeByCode = function(aCode, cb) {
  return this.deleteOne({ code: aCode }, cb)
}

module.exports = mongoose.model('Bicicleta', bicicletaSchema)

/*let Bicicleta = function(id, color, modelo, ubicacion) {
  this.id = id;
  this.color = color;
  this.modelo = modelo;
  this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = () => `id: ${this.id} | color: ${this.color}`

Bicicleta.allBics = []
Bicicleta.add = (bic) => Bicicleta.allBics.push(bic)

Bicicleta.findById = (id) => {
  let bici = Bicicleta.allBics.find(x => x.id == id)
  if(bici) {
    return bici
  }
  else {
    throw new Error(`No existe una bicicleta con el id ${id}`)
  }
}

Bicicleta.removeById = (id) => {
  //Bicicleta.findById(id)
  for(let i = 0; i < Bicicleta.allBics.length; i++) {
    if(Bicicleta.allBics[i].id == id) {
      Bicicleta.allBics.splice(i, 1)
      break
    }
  }
}

let a = new Bicicleta(1, 'rojo', 'urbana', [-16.50399, -68.1297908])
let b = new Bicicleta(2, 'blanca', 'urbana', [-16.5038125, -68.1307134])

Bicicleta.add(a)
Bicicleta.add(b)

module.exports = Bicicleta*/