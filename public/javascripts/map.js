let mymap = L.map('mapid').setView([-16.5038006, -68.129545], 18);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

//L.marker([-16.5038006, -68.129545]).addTo(mymap).bindPopup('Informática.<br>UMSA').openPopup();

$.ajax({
  dataType: "json",
  url: "api/bicicletas",
  success: function(result) {
    console.log(result)
    result.bicicletas.forEach(bici => {
      L.marker(bici.ubicacion, { title: bici.id }).addTo(mymap)
    })
  }
})