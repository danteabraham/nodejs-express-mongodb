let express = require('express')
let router = express.Router()
let usuarioController = require('../../controllers/api/usuarioControllerAPI')


router.get('/', usuarioController.usuariosList)
router.post('/create', usuarioController.usuariosCreate)
router.post('/reservar', usuarioController.usuariosReservar)

module.exports = router
