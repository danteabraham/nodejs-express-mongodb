let express = require('express')
let router = express.Router()
let bicicletaController = require('../controllers/bicicletaController')


router.get('/', bicicletaController.bicicletaList)
router.get('/create', bicicletaController.bicicletaCreate)
router.post('/create', bicicletaController.bicicletaPost)
router.post('/:id/delete', bicicletaController.bicicletaDelete)
router.get('/:id/update', bicicletaController.bicicletaUpdate)
router.post('/:id/update', bicicletaController.bicicletaUpdatePost)

module.exports = router