let Bicicleta = require('../../models/bicicleta')
let request = require('request')
let server = require('../../bin/www')


describe('Bicicleta API', () => {
  describe('GET BICICLETAS /', () => {
    it('status 200', () => {
      expect(Bicicleta.allBics.length).toBe(0)

      var a = new Bicicleta(1, 'rojo', 'urbana', [-16.50399, -68.1297908])
      Bicicleta.add(a)

      request.get('http://localhost:3000/api/bicicletas', function(error, response, body) {
        expect(response.statusCode).toBe(200)
      })
    })
  })

  describe('POST BICICLETAS /create', () => {
    it('status 200', (done) => {
      let headers = { 'content-type': 'application/json' }
      let a = { "id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -16 }

      request.post(
        { headers: headers, url: 'http://localhost:3000/api/bicicletas/create', body: a },
        function(error, response, body) {
          expect(response.statusCode).toBe(200)
          expect(Bicicleta.findById(10).color).toBe("rojo")
          done()
        }
      )
    })
  })
})