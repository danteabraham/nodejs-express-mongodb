let mongoose = require('mongoose')
const { compile } = require('pug')
const bicicleta = require('../../models/bicicleta')
let Bicicleta = require('../../models/bicicleta')


describe('Testing bicicletas', function() {
  beforeEach(function(done) {
    var mongoDB = 'mongodb://localhost/testdb'
    mongoose.connect(mongoDB, { useUnifiedTopology: true, useNewUrlParser: true })

    const db = mongoose.connection
    db.on('error', console.error.bind(console, 'MongoDB connection error'))
    db.once('open', function() {
      console.log('We are connected to testing db')
      done()
    })
  })

  afterEach(function(done) {
    Bicicleta.deleteMany({}, function(err, success) {
      if(err) console.log(err)
      done()
    })
  })

  describe('Bicicleta.createInstance', () => {
    it('Crea una instancia de bicicleta', () => {
      var bici = Bicicleta.createInstance(1, 'verde', 'urbana', [-14.45567, -64.4456])

      expect(bici.code).toBe(1)
      expect(bici.color).toBe('verde')
      expect(bici.modelo).toBe('urbana')
      expect(bici.ubicacion[0]).toBe(-14.45567)
      expect(bici.ubicacion[1]).toBe(-64.4456)
    })
  })

  describe('Bicicleta.allBicis', () => {
    it('Comienza vacia', (done) => {
      Bicicleta.allBicis(function(err, bicis) {
        expect(bicis.length).toBe(0)
        done()
      })
    })
  })

  describe('Bicicleta.add', () => {
    it('Agrega solo una bici', (done) => {
      var aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" })
      Bicicleta.add(aBici, function(err, newBici) {
        if(err) console.log(err)
        Bicicleta.allBicis(function(err, bicis) {
          expect(bicis.length).toEqual(1)
          expect(bicis[0].code).toEqual(aBici.code)
          done()
        })
      })
    })
  })

  describe('Bicicleta.findByCode', () => {
    it('Debe devolver la bici con code 1', (done) => {
      Bicicleta.allBicis(function(err, bicis) {
        expect(bicis.length).toBe(0)

        var aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" })
        Bicicleta.add(aBici, function(err, newBici) {
          if(err) console.log(err)

          var aBici2 = new Bicicleta({ code: 2, color: "roja", modelo: "urbana" })
          Bicicleta.add(aBici2, function(err, newBici) {
            if(err) console.log(err)

            Bicicleta.findByCode(1, function(err, targetBici) {
              expect(targetBici.code).toBe(aBici.code)
              expect(targetBici.color).toBe(aBici.color)
              expect(targetBici.modelo).toBe(aBici.modelo)
              done()
            })
          })
        })
      })
    })
  })

})
// Ya que a cada test, se realiza uno detras de otro, decimos que a cada iteracion el vector que contiene
// a todas las bicis se resetee, asi podra pasar los test y no dar error
/* 
beforeEach(function() {
  console.log('testeando...');}
);*/
/*
beforeEach(() => Bicicleta.allBics = [])

describe('Bicicleta.allBics', () => {
  it('Comienza vacia', () => {
    // Debe comenzar con 0 elmentos
    expect(Bicicleta.allBics.length).toBe(0)
  })
})

describe('Bicicleta.add', () => {
  it('Agregamos una', () => {
    // condicion previa
    expect(Bicicleta.allBics.length).toBe(0)
    // se carga una
    var a = new Bicicleta(1, 'rojo', 'urbana', [-16.50399, -68.1297908])
    Bicicleta.add(a)
    // condicion futura, debe contener al elemento agregado
    expect(Bicicleta.allBics.length).toBe(1)
    expect(Bicicleta.allBics[0]).toBe(a)
  })
})

describe('Bicicleta.findById', () => {
  it('Debe devolver la bici con el id 1', () => {
    // Debe comenzar con 0 elmentos
    expect(Bicicleta.allBics.length).toBe(0)
    // agregamos 2 bicis para probar
    let a = new Bicicleta(1, 'rojo', 'urbana')
    let b = new Bicicleta(2, 'blanca', 'urbana')
    Bicicleta.add(a)
    Bicicleta.add(b)
    // revisamos si la busqueda funciona, comparando cada elemento de la bici encontrada con los valores reales de esa bici
    var targetBici = Bicicleta.findById(1)
    expect(targetBici.id).toBe(1)
    expect(targetBici.color).toBe(a.color)
    expect(targetBici.modelo).toBe(a.modelo)
  })
})*/