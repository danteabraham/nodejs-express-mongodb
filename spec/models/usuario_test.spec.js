let mongoose = require('mongoose')
let Bicicleta = require('../../models/bicicleta')
let Usuario = require('../../models/usuario')
let Reserva = require('../../models/reserva')


describe('Testing usuarios', function() {
  beforeEach(function(done) {
    var mongoDB = 'mongodb://localhost/testdb'
    mongoose.connect(mongoDB, { useUnifiedTopology: true, useNewUrlParser: true })

    const db = mongoose.connection
    db.on('error', console.error.bind(console, 'MongoDB connection error'))
    db.once('open', function() {
      console.log('We are connected to testing db')
      done()
    })
  })

  afterEach(function(done) {
    Reserva.deleteMany({}, function(err, success) {
      if(err) console.log(err)
      Usuario.deleteMany({}, function(err, success) {
        if(err) console.log(err)
        Bicicleta.deleteMany({}, function(err, success) {
          if(err) console.log(err)
          done()
        })
      })  
    })
  })

  describe('Cuando un usuario reserva una bici', () => {
    it('Debe existir la reserva', (done) => {
      const usuario = new Usuario({ nombre: 'Dan' })
      usuario.save()

      const bicicleta = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" })
      bicicleta.save()

      let hoy = new Date()
      let mañana = new Date()
      mañana.setDate(hoy.getDate() + 1)
      usuario.reservar(bicicleta.id, hoy, mañana, function(err, reserva) {
        Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas) {
          console.log(reservas[0])
          expect(reservas.length).toBe(1)
          expect(reservas[0].diasDeReserva()).toBe(2)
          expect(reservas[0].bicicleta.code).toBe(1)
          expect(reservas[0].usuario.nombre).toBe(usuario.nombre)
          done()
        })
      })  
    })
  })

})
